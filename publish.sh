#!/bin/bash

# Upload the sources to our publication server.

# CONFIGURATION ###############################################################

USER=firedrop
SERVER=ftpperso.free.fr


# COLORS ######################################################################

Off='\033[0m'             # Text Reset

# Regular Colors
Black='\033[0;30m'        # Black
Red='\033[0;31m'          # Red
Green='\033[0;32m'        # Green
Yellow='\033[0;33m'       # Yellow
Blue='\033[0;34m'         # Blue
Purple='\033[0;35m'       # Purple
Cyan='\033[0;36m'         # Cyan
White='\033[0;37m'        # White

# Bold
BBlack='\033[1;30m'       # Black
BRed='\033[1;31m'         # Red
BGreen='\033[1;32m'       # Green
BYellow='\033[1;33m'      # Yellow
BBlue='\033[1;34m'        # Blue
BPurple='\033[1;35m'      # Purple
BCyan='\033[1;36m'        # Cyan
BWhite='\033[1;37m'       # White


# ASK FOR PASSWORD ############################################################

echo -e \
"${Yellow}Password for" \
"${Purple}${USER}${Yellow}@${Purple}${SERVER}${Yellow}?${Off}"
read -s PASSWORD


# LOCAL CLEANUP ###############################################################

echo -e "${Yellow}Cleaning local files...${Off}"


# REMOTE CLEANUP ##############################################################

echo -e "${Yellow}Cleaning remote files...${Off}"

# Crappy -e (delete) option for mirror won't recurse, so we delete ourselves
lftp ftp://${USER}:${PASSWORD}@${SERVER} -e \
     "glob -a rm -r * ; quit"

rc=$?
if [ ${rc} -ne 0 ]; then
    echo -e "${BRed}Either the server is down or the pass is wrong.${Off}" >&2
    exit 1
fi


# UPLOAD ######################################################################

echo -e "${Yellow}Uploading files...${Off}"

# Additional mirror option:
# -e to delete remote files, but it won't recurse (bug?)
# -x ignored_dir
lftp ftp://${USER}:${PASSWORD}@${SERVER} -e \
     "mirror -R --parallel=1 build/ / ; quit"


# DONE ########################################################################

echo -e "${BGreen}Done !${Off}"