#!/bin/bash

# Please note that we're using a custom build of wget, because of the bogusness
# of the way it detects html files that were already downloaded.
# If the file does not end by .html (and many of ours don't), it won't parse it
# and add its anchors to its internal list of pages to fetch.
# Therefore, if you use the default wget and want to pause and resume the
# download, you're screwed without a patch.
# Otherwise, it will work fine as a one-shot, with no interruptions.

# This script is very much tied to the architecture of whyweprotest.net
# This is a quick hack that will need a lot of work to run with other forums.

# Note that it also downloads extra? stuff in a `community` directory.
# We manually merged `community/threads` into `threads` after the download.

#wget="/usr/local/bin/wget"
wget="~/Apps/wget/bin/wget"

website="https://whyweprotest.net"
#website="https://whyweprotest.net/forums/media.216/page-100"

website_login_page="https://whyweprotest.net/login"
website_login_post="https://whyweprotest.net/login/login"

regex="(threads|forums)/[^/]+/(index\.html|page-[0-9]+)?/?$"

echo -e "Username (your email)?"
read login
echo -e "Password?"
read -s password
echo -e "Thank you!\n"

rm login*

# Grab the cookies from the login page (we NEED to do that)
$wget --save-cookies=cookies --keep-session-cookies $website_login_page

# Post our login credentials, and store the response cookies
$wget --load-cookies=cookies --save-cookies=cookies --keep-session-cookies                   \
     --post-data="register=0&remember=1&cookie_check=1&login=$login&password=$password"      \
     $website_login_post

# Now, we can leech as a connected user, yay!
$wget --tries=3 --quota=0                                                                    \
     --no-verbose                                                                            \
     --limit-rate=800k --wait=0.01 --random-wait  `# be gentle with the webserver`           \
     --user-agent=Mozilla --no-check-certificate                                             \
     --recursive --level=0                                                                   \
     --accept-regex=$regex --regex-type=pcre      `# just download what we want, not all`    \
     --ignore-case                                                                           \
     --continue --no-clobber                      `# so we can stop and resume later`        \
     --restrict-file-names=nocontrol              `# or wget will segfault on russian chars` \
     --load-cookies=cookies                       `# login using cookies`                    \
     $website

echo -e "\nDone!"