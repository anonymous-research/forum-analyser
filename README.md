A forum analysis generator made of dirty but handy scripts.

The only targeted website for now is whyweprotest.net but we aim to add others.


Ideas
-----

- word cloud, word histogram
- user network graph : connections via shared threads and direct replies


Usage
=====

Install
-------

    pip install flask peewee bs4 colorama termcolor


Leech
-----

    bash download.sh

You cannot pause and resume this script because of a _"bug"_ in `wget`.
You need to run it in one shot, or patch `wget`.


Curate (aka scrape)
-------------------

    python curate.py


Inspect
-------

    python birdview.py


Visualize
---------

    python web/index.py

Then, visit http://localhost:5000


Changelog
=========

0.1.0
-----

- Add a calendar display for regex queries on forum, thread, author, or content
- Add a way to generate the static HTML/CSS/JS files of the website

