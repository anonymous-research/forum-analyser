#!/usr/bin/python

from playhouse.sqlite_ext import SqliteExtDatabase
from colorama import init
from termcolor import colored, cprint

import sys
import codecs

# Enforce UTF-8, so that we can pipe (>) the stdout to a file
sys.stdout = codecs.getwriter('utf-8')(sys.stdout)

# use Colorama to make Termcolor work on all platforms
init()

cprint("Crunching a bird view for whyweprotest.net...", "green")

db = SqliteExtDatabase("wwp_20161228_105253.db")
db.connect()


def count(what):  # We don't care about SQL injections here :]
    for countResponse in db.execute_sql("SELECT COUNT(*) FROM %s" % what):
        return countResponse[0]  # I MUST not be doing this right...


print(colored("%d"%count('User'),   "yellow") + " users")
print(colored("%d"%count('Forum'),  "yellow") + " forums")
print(colored("%d"%count('Thread'), "yellow") + " threads")
print(colored("%d"%count('Post'),   "yellow") + " posts")


cprint("\nTop posts by word count :", "green")
sql = """
SELECT p.id, p.words, t.id, t.title, t.url
FROM Post p
JOIN Thread t
WHERE t.id = p.thread_id
ORDER BY p.words DESC
LIMIT 25
"""
for post in db.execute_sql(sql):
    print("%d words in %s (%s)" % (post[1], colored(post[3], "yellow"), post[4]))


cprint("\nTop threads by posts :", "green")
sql = """
SELECT
  t.id,
  t.title,
  t.url,
  (SELECT COUNT(p.id) FROM Post p WHERE t.id = p.thread_id) AS cnt
FROM Thread t
ORDER BY cnt DESC
LIMIT 25
"""
for row in db.execute_sql(sql):
    print("%d posts in %s (%s)" % (row[3], colored(row[1], "yellow"), row[2]))


cprint("\nTop threads by word count :", "green")
sql = """
SELECT
  t.id,
  t.title,
  t.url,
  (SELECT SUM(p.words) FROM Post p WHERE t.id = p.thread_id) words
FROM Thread t
ORDER BY words DESC
LIMIT 25
"""
for row in db.execute_sql(sql):
    print("%d words in %s (%s)" % (row[3], colored(row[1], "yellow"), row[2]))


cprint("\nTop threads by likes :", "green")
sql = """
SELECT
  t.id,
  t.title,
  t.url,
  (SELECT SUM(p.likes) FROM Post p WHERE t.id = p.thread_id) likes
FROM Thread t
ORDER BY likes DESC
LIMIT 25
"""
for row in db.execute_sql(sql):
    print("%s likes in %s (%s)" % (row[3], colored(row[1], "yellow"), row[2]))


cprint("\nTop threads by likes per posts ratio :", "green")
sql = """
SELECT
  t.id,
  t.title,
  t.url,
  (SELECT SUM(p.likes) FROM Post p WHERE t.id = p.thread_id) likes,
  (SELECT COUNT(p.id)  FROM Post p WHERE t.id = p.thread_id) posts
FROM Thread t
WHERE posts > 10 AND likes > 0
ORDER BY 100 * likes / posts DESC
LIMIT 30
"""

ratio = []
for row in db.execute_sql(sql):
    likes = row[3]
    posts = row[4]
    if posts > 0:
        ratio.append((row[0], row[1], row[2], likes, posts, likes / posts))
# ratio.sort(key=lambda x: x[4], reverse=True)
for j in ratio:
    print("%d likes(%d)/post(%d) in %s (%s)" % (j[5], j[3], j[4], colored(j[1], "yellow"), j[2]))


cprint("\nTop threads by anonymous ratio :", "green")
sql = """
SELECT
  t.id,
  t_posts.cnt AS posts,
  t_anons.cnt AS anons,
  100 * t_anons.cnt / t_posts.cnt AS ratio,
  t.title,
  t.url
FROM
  (
    SELECT t.id AS id, COUNT(p.id) AS cnt
    FROM   Post p, Thread t
    WHERE  t.id = p.thread_id
    GROUP BY t.id
    ORDER BY t.id
  ) t_posts,
  (
    SELECT t.id AS id, COUNT(p.id) AS cnt
    FROM   Post p, Thread t, User u
    WHERE  u.id = p.user_id
    AND    t.id = p.thread_id
    AND    u.name = "Anonymous"
    GROUP BY t.id
    ORDER BY t.id
  ) t_anons,
  Thread t
WHERE t_posts.id = t_anons.id
AND t.id = t_posts.id
AND posts > 7
ORDER BY ratio DESC, posts DESC
LIMIT 42;
"""

for row in db.execute_sql(sql):
    print("%d%% anons for %d posts in %s (%s)" % (row[3], row[1], colored(row[4], "yellow"), row[5]))





# nombre de mots par post, par thread -
# nombre d'auteurs par threads-
# nombre de post par thread-
# date des posts (notament dates premiers et derniers post d un thread pour connaitre sa life.b. -
