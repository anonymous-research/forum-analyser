import sys
from os.path import isfile, isdir, isabs, join, abspath, dirname, basename
from flask_frozen import Freezer
from index import app, get_days_for_query
import yaml
from colorama import init
from termcolor import colored, cprint

# use Colorama to make Termcolor work on all platforms
init()

# PATH RELATIVITY #############################################################
root_dir = dirname(dirname(abspath(__file__)))


def get_path(relative_path):
    return abspath(join(root_dir, relative_path))


# CONFIGURATION ###############################################################

# There's no YAML support in Flask (yet) so we make our own.
with open(get_path('config.yml')) as f:
    config = yaml.load(f)


# FREEZER #####################################################################

freezer = Freezer(app)


@freezer.register_generator
def generate_queries():
    for endpoint in ['calendar', 'histogram', 'calendar_csv']:
        for query in config['queries']['calendar']:
            yield (endpoint, {'query': query['slug']})
    for endpoint in ['users', 'users_csv']:
        for query in config['queries']['users']:
            yield (endpoint, {'query': query['slug']})


@freezer.register_generator
def generate_queries_per_day():
    for endpoint in ['posts_per_day_html']:
        for query in config['queries']['calendar']:
            for day in get_days_for_query(query['slug'], 'calendar'):
                print("Freezing posts for %s, the %s." % (
                    colored(query['slug'], 'yellow'),
                    colored(day, 'magenta')
                ))
                yield (endpoint, {
                    'query': query['slug'],
                    'day': day,
                })


if __name__ == '__main__':
    if len(sys.argv) > 1 and sys.argv[1] == 'serve':
        freezer.serve()
    else:
        freezer.freeze()
    cprint('Done!', 'green')
