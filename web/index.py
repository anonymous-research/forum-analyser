from os.path import isfile, isdir, isabs, join, abspath, dirname, basename
from flask import Flask, url_for, request, abort, send_from_directory, jsonify
from jinja2 import Environment, FileSystemLoader
import re
import csv
import yaml
import random
import datetime

# PATH RELATIVITY #############################################################
root_dir = dirname(dirname(abspath(__file__)))


def get_path(relative_path):
    return abspath(join(root_dir, relative_path))


# VERSION AND OTHER SYSTEM INFORMATION ########################################

with open(join(root_dir, 'VERSION'), 'r') as version_file:
    version = version_file.read().strip()

# CONFIGURATION ###############################################################

# There's no YAML support in Flask (yet) so we make our own.
with open(get_path('config.yml')) as f:
    config = yaml.load(f)

# SETUP FLASK ENGINE ##########################################################

app = Flask("OP-WhyWeProtest")


# SETUP JINJA2 TEMPLATE ENGINE ################################################

def markdown_filter(value, nl2br=False, p=True):
    """
    nl2br: set to True to replace line breaks with <br> tags
    p: set to False to remove the enclosing <p></p> tags
    """
    from markdown import markdown
    from markdown.extensions.nl2br import Nl2BrExtension
    from markdown.extensions.abbr import AbbrExtension
    extensions = [AbbrExtension()]
    if nl2br is True:
        extensions.append(Nl2BrExtension())
    # value = value.replace(ur'\u2191', "")  # remove the quote arrow

    markdowned = markdown(value, output_format='html5', extensions=extensions)
    if p is False:
        markdowned = markdowned.replace(r"<p>", "").replace(r"</p>", "")
    return markdowned


def shuffle_filter(seq):
    """
    This shuffles the sequence it is applied to.
    'tis a failure of jinja2 to not provide a shuffle filter by default.
    """
    try:
        result = list(seq)
        random.shuffle(result)
        return result
    except:
        return seq


tpl_engine = Environment(
    loader=FileSystemLoader([get_path('view')]),
    trim_blocks=True,
    lstrip_blocks=True,
)

tpl_engine.globals.update(
    url_for=url_for,
)

tpl_engine.filters['md'] = markdown_filter
tpl_engine.filters['markdown'] = markdown_filter
tpl_engine.filters['shuffle'] = shuffle_filter

tpl_global_vars = {
    'app': app,
    'config': config,
    'request': request,
    'version': version,
    'now': datetime.datetime.now(),
}


# HELPERS #####################################################################

def render_view(view, context=None):
    """
    A simple helper to render [view] template with [context] vars.
    It automatically adds the global template vars defined above, too.
    It returns a string, usually the HTML contents to display.

    We're not using flask's render_template because we want more flexibility.
    """
    if context is None:
        context = {}
    return tpl_engine.get_template(view).render(
        dict(tpl_global_vars.items() + context.items())
    )


def connect_db():
    from playhouse.sqlite_ext import SqliteExtDatabase
    # _db = SqliteExtDatabase("wwp_20161228_105253.db")
    # _db = SqliteExtDatabase("wwp_20170120_011737.db")
    _db = SqliteExtDatabase("wwp_20170120_214326.db")
    _db.connect()
    return _db


def require_safe_query(query, section='calendar'):
    if section not in config['queries']:
        abort(500, "No section named '%s' in config.yml queries." % section)
    queries = config['queries'][section]
    query_safe = None
    for allowedQuery in queries:
        if allowedQuery['slug'] == query:
            query_safe = allowedQuery
            break
    if not query_safe:
        abort(404, "No query '%s' in %s in config.yml." % (query, section))
    if not 'filters' in query_safe:
        query_safe['filters'] = dict()
    if not 'content' in query_safe['filters']:
        query_safe['filters']['content'] = ".*"
    if not 'author' in query_safe['filters']:
        query_safe['filters']['author'] = ".*"
    if not 'thread' in query_safe['filters']:
        query_safe['filters']['thread'] = ".*"
    if not 'forum' in query_safe['filters']:
        query_safe['filters']['forum'] = ".*"
    return query_safe


def list_of_days(start_day, end_day):
    day_format = "%Y-%m-%d"
    start = datetime.datetime.strptime(start_day, day_format)
    end = datetime.datetime.strptime(end_day, day_format)
    days = (end - start).days
    for x in range(days):
        yield datetime.datetime.strftime(
            start + datetime.timedelta(days=x),
            day_format
        )


def get_days_for_query(query, section):
    query_safe = require_safe_query(query, section)
    sql = """
    SELECT DATE(p.created_at) AS day
    FROM Post p, User u, Thread t, Forum f
    WHERE u.id = p.author_id
    AND t.id = p.thread_id
    AND f.id = t.forum_id
    AND (p.content REGEXP {var})
    AND (u.name REGEXP {var})
    AND (t.title REGEXP {var})
    AND (f.title REGEXP {var})
    GROUP BY day
    ORDER BY day;
    """.format(var=db.interpolation)
    rows = db.execute_sql(sql, (
        query_safe['filters']['content'],
        query_safe['filters']['author'],
        query_safe['filters']['thread'],
        query_safe['filters']['forum']
    ))

    return (row[0] for row in rows)

# ROUTES ######################################################################

db = connect_db()  # trying to see if this optimizes freezing


@app.route("/")
def home():
    return render_view('home.html.jinja2', {})


@app.route("/calendar/<query>.html")
def calendar(query):
    # queries are defined in config.yml
    query_safe = require_safe_query(query, 'calendar')

    return render_view('calendar.html.jinja2', {
        'query': query_safe
    })


@app.route("/histogram/<query>.html")
def histogram(query):
    return render_view('histogram.html.jinja2', {
        'query': require_safe_query(query, 'calendar')
    })


@app.route("/users/<query>.html")
def users(query):
    return render_view('users.html.jinja2', {
        'query': require_safe_query(query, 'users')
    })


@app.route("/calendar.<query>.csv")
def calendar_csv(query):
    query_safe = require_safe_query(query, 'calendar')
    query_name = query_safe['name']
    # query_regex = query_safe['regex']
    query_ratio_deprecated = request.args.get('ratio')
    query_file = query
    if query_ratio_deprecated:
        query_file += '_ratio'

    filename = "calendar.%s.csv" % query_file
    pathname = get_path('data/' + filename)
    if not isfile(pathname):
        # db = connect_db()

        sql = """
        SELECT DATE(p.created_at) AS day, COUNT(*)
        FROM Post p, User u, Thread t, Forum f
        WHERE u.id = p.author_id
        AND t.id = p.thread_id
        AND f.id = t.forum_id
        AND (p.content REGEXP {var})
        AND (u.name REGEXP {var})
        AND (t.title REGEXP {var})
        AND (f.title REGEXP {var})
        GROUP BY day
        ORDER BY day;
        """.format(var=db.interpolation)
        rows = db.execute_sql(sql, (
            query_safe['filters']['content'],
            query_safe['filters']['author'],
            query_safe['filters']['thread'],
            query_safe['filters']['forum']
        ))

        # SELECT COUNT(created_at) FROM Post WHERE DATETIME(created_at) < DATETIME("2008-04-01 00:00:00") ORDER BY created_at ASC;

        days = dict()
        for day in list_of_days("2008-01-01", "2016-11-30"):
            days[day] = 0

        for row in rows:
            day = row[0]
            days[day] = row[1]

        if 'ratio' in query_safe:
            query_ratio = require_safe_query(query_safe['ratio'], 'calendar')
            if 'ratio' in query_ratio:
                abort(401, "Sylvian broke everything!")
            sql = """
            SELECT COUNT(*), p.created_at, DATE(p.created_at) AS day
            FROM Post p, User u, Thread t, Forum f
            WHERE u.id = p.author_id
            AND t.id = p.thread_id
            AND f.id = t.forum_id
            AND (p.content REGEXP {var})
            AND (u.name REGEXP {var})
            AND (t.title REGEXP {var})
            AND (f.title REGEXP {var})
            GROUP BY day
            ORDER BY day;
            """.format(var=db.interpolation)
            rows = db.execute_sql(sql, (
                query_ratio['filters']['content'],
                query_ratio['filters']['author'],
                query_ratio['filters']['thread'],
                query_ratio['filters']['forum']
            ))
            for row in rows:
                cnt = row[0]
                day = row[1][0:10]
                if (cnt > 0) and (day in days):
                    days[day] = (1000.0 * days[day]) / float(cnt)

        if query_ratio_deprecated:
            sql = """
            SELECT COUNT(created_at), created_at, DATE(created_at) AS day
            FROM Post
            GROUP BY day;
            """.format(var=db.interpolation)
            postsPerDay = db.execute_sql(sql)

            for row in postsPerDay:
                cnt = row[0]
                day = row[1][0:10]
                if (cnt > 0) and (day in days):
                    days[day] = (1000.0 * days[day]) / float(cnt)
                    # days[day] = cnt

        fe = open(pathname, "w")
        fe.write("day,count\n")
        for day in sorted(days.keys()):
            fe.write("%s,%d\n" % (day, days[day]))
        fe.close()

    return send_from_directory('data', filename)


def posts_per_day(query, day):
    """
    :param query: defined in config.yml
    :param day: eg. 2916-12-29
    """
    # db = connect_db()
    query_safe = require_safe_query(query, 'calendar')
    if not re.match("^[0-9]{4}-[0-9]{2}-[0-9]{2}$", day):
        abort(401, "Day '%s' invalid." % day)

    sql = """
    SELECT t.title, u.name, p.content, p.created_at, p.url, f.title,
           DATE(created_at) AS day_created_at
    FROM Post p, User u, Thread t, Forum f
    WHERE u.id = p.author_id
    AND t.id = p.thread_id
    AND f.id = t.forum_id
    AND day_created_at = {var}
    """
    sql_vars = (day,)
    for where in [
        ('content', 'p.content'),
        ('author',  'u.name'),
        ('thread',  't.title'),
        ('forum',   'f.title'),
    ]:
        filter_key, sql_key = where
        if filter_key in query_safe['filters']:
            filter_regex = query_safe['filters'][filter_key]
            if filter_regex != ".*":
                sql += " AND (%s REGEXP {var}) " % sql_key
                sql_vars = sql_vars + (filter_regex,)

    sql += """
    ORDER BY f.title, t.title, p.created_at
    """
    sql = sql.format(var=db.interpolation)
    rows = db.execute_sql(sql, sql_vars)

    posts = []
    for row in rows:
        content = row[2]
        if query_safe['filters']['content'] != ".*":
            # Oddity: wrap with parentheses to get \1 because \0 is empty ? WTF
            search = re.compile("(" + query_safe['filters']['content'] + ")",
                                re.IGNORECASE)
            content = re.sub(search, r'**\1**', content)
        posts.append({
            'thread': row[0],
            'author': row[1],
            'content': content,
            'created_at': row[3],
            'url': row[4],
            'forum': row[5]
        })

    return posts


@app.route("/posts/<query>/day/<day>.json")
def posts_per_day_json(query, day):
    """
    :param query: defined in config.yml
    :param day: eg. 2916-12-29
    """
    return jsonify(posts_per_day(query, day))


@app.route("/posts/<query>/day/<day>.html")
def posts_per_day_html(query, day):
    """
    :param query: defined in config.yml
    :param day: eg. 2916-12-29
    """
    return render_view('xhr/posts_per_day.html.jinja2', {
        'day': day,
        'posts': posts_per_day(query, day)
    })


@app.route("/users.<query>.csv")
def users_csv(query):
    query_safe = require_safe_query(query, 'users')
    query_file = query

    filename = "users.%s.csv" % query_file
    pathname = get_path('data/' + filename)
    if not isfile(pathname):

        sql = """
        SELECT u.name, COUNT(p.id) as posts_count
        FROM Post p, User u, Thread t, Forum f
        WHERE u.id = p.author_id
        AND   t.id = p.thread_id
        AND   f.id = t.forum_id
        """
        sql_vars = ()
        for where in [
            ('content', 'p.content'),
            ('author', 'u.name'),
            ('thread', 't.title'),
            ('forum', 'f.title'),
        ]:
            filter_key, sql_key = where
            if filter_key in query_safe['filters']:
                filter_regex = query_safe['filters'][filter_key]
                if filter_regex != ".*":
                    sql += " AND (%s REGEXP {var}) " % sql_key
                    sql_vars = sql_vars + (filter_regex,)

        sql += """
        GROUP BY u.id
        ORDER BY posts_count DESC, u.name
        """
        sql = sql.format(var=db.interpolation)
        rows = db.execute_sql(sql, sql_vars)

        data = list(rows)

        if 'ratio' in query_safe:
            query_ratio = require_safe_query(query_safe['ratio'], 'users')
            if 'ratio' in query_ratio:
                abort(401, "Can't use a ratio'ed query as a ratio!")
            sql = """
            SELECT COUNT(p.id)
            FROM Post p, User u, Thread t, Forum f
            WHERE u.id = p.author_id
            AND   t.id = p.thread_id
            AND   f.id = t.forum_id
            """
            sql_vars = ()
            for where in [
                ('content', 'p.content'),
                ('author', 'u.name'),
                ('thread', 't.title'),
                ('forum', 'f.title'),
            ]:  # <= sad face because of crappy indentation
                filter_key, sql_key = where
                if filter_key in query_safe['filters']:
                    filter_regex = query_safe['filters'][filter_key]
                    if filter_regex != ".*":
                        sql += " AND (%s REGEXP {var}) " % sql_key
                        sql_vars = sql_vars + (filter_regex,)

            sql = sql.format(var=db.interpolation)
            rows = db.execute_sql(sql, sql_vars)
            denominator = list(rows)[0][0]

            for k, v in enumerate(data):
                data[k] = (v[0], 1.0 * v[1] / denominator)

        fe = open(pathname, "w")
        wr = csv.writer(fe, quoting=csv.QUOTE_NONNUMERIC)
        wr.writerow(('user', 'posts'))
        for d in data:
            wr.writerow((d[0].encode('utf-8'), d[1]))
        fe.close()

    return send_from_directory('data', filename)


if __name__ == "__main__":
    # We add the YAML config file to the list of files to watch for automatic
    # reload of the development server.
    extra_files = [get_path('config.yml')]
    app.run(debug=True, extra_files=extra_files)
