#!/usr/bin/python
# coding=utf-8
from bs4 import CData
from bs4 import NavigableString
from bs4 import Tag
from peewee import *
from playhouse.sqlite_ext import SqliteExtDatabase
from bs4 import BeautifulSoup
import datetime
import os
import re
from colorama import init
from termcolor import colored, cprint

# use Colorama to make Termcolor work on all platforms
init()

now = datetime.datetime.now().strftime('%Y%m%d_%H%M%S')
db = SqliteExtDatabase("wwp_%s.db" % now)


class BaseModel(Model):
    class Meta:
        database = db


class User(BaseModel):
    name = CharField(unique=True)


class Forum(BaseModel):
    title = CharField()
    url = CharField()


class Thread(BaseModel):
    title = CharField()
    forum = ForeignKeyField(Forum, related_name='threads')
    url = CharField()


class Post(BaseModel):
    author = ForeignKeyField(User, related_name='posts')
    thread = ForeignKeyField(Thread, related_name='posts')
    content = TextField()
    likes = IntegerField()
    words = IntegerField()
    url = CharField()
    created_at = DateTimeField(default=datetime.datetime.now)


db.connect()
db.create_tables([User, Forum, Thread, Post])


def count_words(text):
    regex = re.compile("^\w+.*")
    return len([w for w in text.split() if regex.match(w)])


print("Curating data from the webpages...")

forumCache = dict()
threadCache = dict()
userCache = dict()

for root, dirnames, filenames in os.walk("whyweprotest.net/threads"):
    path = root.split('/')
    for filename in filenames:
        f = os.path.join(root, filename)
        soup = BeautifulSoup(open(f), "lxml", from_encoding="utf-8")

        line = ""

        # Forum
        forum_crumbs = []
        forum_url = ""
        for crust in soup.select(".breadBoxTop fieldset.breadcrumb .crumbs .crust"):
            forum_crumbs.append(crust.select_one("a span[itemprop]").string)
            if not forum_url:
                forum_url = crust.select_one("a").get("href")
        if not len(forum_crumbs) >= 3:
            exit("No forum title for %s." % f)
        forum_title = ' '.join(forum_crumbs[2:])
        forum_title = 'WhyWeProtest ' + forum_title
        line += "(%s) " % forum_title
        forum = forumCache.get(forum_title)
        if forum is None:
            forum = Forum.create(title=forum_title, url=forum_url)
            forumCache[forum_title] = forum

        # Thread
        if not soup.title:
            exit("No title for %s." % f)
        threadTitle = soup.title.string
        threadTitleSuffix = " | Why We Protest | Anonymous Activism Forum"
        m = re.search("^(.+?)(?: \\| Page [0-9]+)?"+re.escape(threadTitleSuffix), threadTitle)
        threadTitle = m.group(1)
        
        line += colored(threadTitle, 'yellow')
        
        thread = threadCache.get(threadTitle)
        if thread is not None:
            line += " #%d" % thread.id
            # print("  Thread already exists with id %d" % thread.id)
        else:
            thread_url = soup.select_one("meta[property=og:url]")
            if thread_url:
                thread_url = thread_url.get('content')
            else:
                # todo: enquire about threads with unknown urls
                thread_url = "unknown"  # yes, this happens, possibly at #47637
            thread = Thread.create(title=threadTitle, url=thread_url, forum=forum)
            threadCache[threadTitle] = thread
            line += " +#%d" % thread.id
            # print("  Created thread with id %d" % thread.id)

        # Posts and their Users
        userNames = []
        postsCount = 0
        with db.atomic():  # bulk insert all posts once per page, for speed
            # for each post in the page
            for msgSoup in soup.select("ol#messageList li.message"):
                # Scrape the username
                userName = msgSoup.get("data-author")
                
                # This looks good but returns a tuple, not an instance of User ?
                # user = User.get_or_create(name=userName)
                
                # Works, but it's slower than our cache, of course
                #user = None
                #try:
                #    user = User.get(User.name == userName)
                #except DoesNotExist:
                #    user = None
                
                user = userCache.get(userName)
                
                if user is None:
                    user = User.create(name=userName)
                    userCache[userName] = user
                
                if userName not in userNames:
                    userNames.append(userName)
                
                contentSoup = msgSoup.select("blockquote.messageText")[0]
                content = ""

                for d in contentSoup.descendants:
                    if type(d) in (NavigableString, CData):
                        content += d.encode('utf-8')
                    elif type(d) is Tag:
                        if d.name == "embed" and 'src' in d.attrs:
                            src = d['src'].encode('utf-8')
                            content += "\n\n [Video](%s) \n\n" % src
                        if d.name == "img" and 'src' in d.attrs:
                            src = d['src'].encode('utf-8')
                            if not src.startswith('http'):
                                src = "http://whyweprotest.net/%s" % src
                            alt = 'img' if 'alt' not in d.attrs else d['alt']
                            alt = re.sub(r"\\[|\\]", "", alt).encode('utf-8')
                            content += "![%s](%s)" % (alt, src)

                # remove the quote arrow
                content = content.replace('↑', "")
                # strip whitespaces (no!)
                # content = re.sub(r"[\t ]+", r" ", content)
                # content = re.sub(r"\s*\n\s*", r"\n", content)

                # Count the words
                words = count_words(content)

                # Grab the creation date
                created_at = msgSoup.select(".messageMeta span.DateTime")
                if len(created_at) > 0:
                    created_at = created_at[0]
                    created_at = created_at.get("title")
                    created_at = datetime.datetime.strptime(created_at, '%b %d, %Y at %I:%M %p')
                else:
                    # some posts have another HTML tag for the datetime ?!
                    created_at = msgSoup.select(".messageMeta abbr.DateTime")
                    if len(created_at) > 0:
                        created_at = created_at[0]
                        created_at = created_at.string
                        created_at = datetime.datetime.strptime(created_at, '%b %d, %Y at %I:%M %p')
                    else:
                        print(colored("  No datetime found for post #%d by %s" % (postsCount+1, user.name), 'red'))
                        if post and post.created_at: # try to use datetime of previous post ?
                            created_at = post.created_at
                        else:
                            created_at = datetime.datetime.now()
                
                # Jun 23, 2011 at 12:05 PM
                #print("Created at %s" % created_at.strftime('%b %d, %Y at %I:%M %p'))
                
                # And now the post URL
                post_url = msgSoup.select_one(".messageMeta a.hashPermalink").get('href')
                if not post_url.startswith("http://") and not post_url.startswith("https://"):
                    post_url = "http://whyweprotest.net/%s" % post_url
                else:
                    cprint("HTTP ALREADY ?! %s" % post_url, "red")

                # And now the likes, if any
                likes = 0
                likesSoup = msgSoup.select(".likesSummary")
                if len(likesSoup) > 0:
                    likes = int(likesSoup[0].select("ul li strong")[0].string)
                
                # Finally, commit the Post with all the data
                post = Post.create(author=user, thread=thread, content=content,
                                   created_at=created_at, url=post_url,
                                   likes=likes, words=words)
                postsCount += 1
            
        line += " +%d by %d" % (postsCount, len(userNames))
        print(line)


db.close()

print(colored("Done.", 'green'))
